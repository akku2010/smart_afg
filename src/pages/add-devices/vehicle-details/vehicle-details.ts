import { Component, OnInit, ViewChild } from "@angular/core";
import { IonicPage, NavParams, AlertController, NavController, ToastController, ModalController, ViewController } from "ionic-angular";
import { ApiServiceProvider } from "../../../providers/api-service/api-service";
import { Chart } from 'chart.js';
import * as moment from 'moment';
import { SocialSharing } from "@ionic-native/social-sharing";
import { DrawerState } from "ion-bottom-drawer";
import { SMS } from "@ionic-native/sms";
import { Clipboard } from '@ionic-native/clipboard';
import * as TinyURL from 'tinyurl';
import { TranslateService } from "@ngx-translate/core";

@IonicPage()
@Component({
  selector: 'page-vehicle-details',
  templateUrl: './vehicle-details.html'
})

export class VehicleDetailsPage implements OnInit {

  @ViewChild('doughnutCanvas') doughnutCanvas;
  @ViewChild('barCanvas') barCanvas;
  @ViewChild('pieCanvas') pieCanvas;
  doughnutChart: any;
  barChart: any;
  pieChart: any;

  vehicleData: any = {};
  fromDate: string;
  toDate: string;
  islogin: any;
  curTime: any;
  data7Days: any = [];
  data7Data: any = [];
  lastStoppedAt: any;
  today_run: any;
  today_stop: any;
  option_switch: boolean;
  showFooter: boolean = false;
  condition: string = 'gpsc';
  condition1: string = 'light';
  condition2: string = 'light';
  tttime: number;
  resToken: any;
  clicked: boolean = false;
  messages: any;
  dataEngine: any;
  DeviceConfigStatus: any;
  immobType: any;


  drawerState1 = DrawerState.Docked;
  dockedHeight1 = 150;
  distanceTop1 = 378;
  minimumHeight1 = 0;
  shouldBounce = true;

  checkedPass: string;
  respMsg: any;
  commandStatus: any;
  intervalID: any;
  timeoutId: any;
  timeoutSeconds: number = 60000;
  currTime: any;
  newDate: any;

  constructor(
    public navParam: NavParams,
    public alertCtrl: AlertController,
    public apiCall: ApiServiceProvider,
    public navCtrl: NavController,
    public toastCtrl: ToastController,
    public modalCtrl: ModalController,
    public viewCtrl: ViewController,
    private socialSharing: SocialSharing,
    public translate: TranslateService,
    private sms: SMS,
    private clipboard: Clipboard
  ) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    this.vehicleData = navParam.get('param');
    this.curTime = moment({ hours: 0 }).format();
    this.option_switch = navParam.get("option_switch");
    let newDate: any = new Date();
    let newd2: any = new Date(newDate - this.vehicleData.today_running).getTime() - new Date(new Date().setHours(0, 0, 0, 0)).getTime();
    this.today_run = this.formatDurationHMS(this.vehicleData.today_running);
    this.today_stop = this.formatDurationHMS(newd2);
    let temp = new Date(this.vehicleData.lastStoppedAt);
    if (this.vehicleData.lastStoppedAt != null) {
      let fd = temp.getTime();
      let td = new Date().getTime();
      let time_difference = td - fd;
      let total_min = time_difference / 60000;
      let hours = total_min / 60
      let rhours = Math.floor(hours);
      let minutes = (hours - rhours) * 60;
      let rminutes = Math.round(minutes);
      this.lastStoppedAt = rhours + ' hrs ' + rminutes;
    } else {
      this.lastStoppedAt = '00' + ' hrs ' + '00';
    }
  }

  dialNumber(number) {
    window.open('tel:' + number, '_system');
  }

  copyText(text) {
    this.clipboard.copy(text);
  }

  ngOnInit() {
    this.getChart();
    this.getChart2()
  }
  measurementUnit: string = 'MKS';
  ionViewDidEnter() {
    this.getDefaultUserSettings();
  }
  user_data: any = {};
  getDefaultUserSettings() {
    let b_url = this.apiCall.mainUrl + "users/get_user_setting";
    let Var = { uid: this.islogin._id };
    this.apiCall.urlpasseswithdata(b_url, Var)
      .subscribe(resp => {
        this.user_data = resp;
        if (resp.unit_measurement !== undefined) {
          this.measurementUnit = resp.unit_measurement;
        } else {
          if (localStorage.getItem('MeasurementType') !== null) {
            let measureType = localStorage.getItem('MeasurementType');
            this.measurementUnit = measureType;
          } else {
            this.measurementUnit = 'MKS';
          }
        }
      },
        err => {
          console.log(err);
          if (localStorage.getItem('MeasurementType') !== null) {
            let measureType = localStorage.getItem('MeasurementType');
            this.measurementUnit = measureType;
          } else {
            this.measurementUnit = 'MKS';
          }
        });
  }
  IgnitionOnOff(d) {
    let that = this;
    if (d.last_ACC != null || d.last_ACC != undefined) {
      if (localStorage.getItem('AlreadyClicked') !== null) {
        let toast = this.toastCtrl.create({
          message: this.translate.instant('Process ongoing..'),
          duration: 1800,
          position: 'middle'
        });
        toast.present();
      } else {
        this.checkImmobilizePassword();
        this.messages = undefined;
        this.dataEngine = d;
        let baseURLp = this.apiCall.mainUrl + 'deviceModel/getDevModelByName?type=' + this.dataEngine.device_model.device_type;
        this.apiCall.startLoading().present();
        this.apiCall.ignitionoffCall(baseURLp)
          .subscribe(data => {
            this.apiCall.stopLoading();
            this.DeviceConfigStatus = data;
            this.immobType = data[0].imobliser_type;

            if (this.dataEngine.ignitionLock == '1') {
              this.messages = this.translate.instant('Do you want to unlock the engine?')
            } else {
              if (this.dataEngine.ignitionLock == '0') {
                this.messages = this.translate.instant('Do you want to lock the engine?')
              }
            }
            let alert = this.alertCtrl.create({
              message: this.messages,
              buttons: [{
                text: 'YES',
                handler: () => {
                  if (this.immobType == 0 || this.immobType == undefined) {
                    localStorage.setItem('AlreadyClicked', 'true');
                    let devicedetail = {
                      "_id": this.dataEngine._id,
                      "engine_status": !this.dataEngine.engine_status
                    }
                    this.apiCall.deviceupdateCall(devicedetail)
                      .subscribe(response => {
                        const toast = this.toastCtrl.create({
                          message: response.message,
                          duration: 2000,
                          position: 'top'
                        });
                        toast.present();
                        let msg;
                        if (!this.dataEngine.engine_status) {
                          msg = this.DeviceConfigStatus[0].resume_command;
                        }
                        else {
                          msg = this.DeviceConfigStatus[0].immoblizer_command;
                        }

                        this.sms.send(d.sim_number, msg);
                        const toast1 = this.toastCtrl.create({
                          message: this.translate.instant('SMS sent successfully'),
                          duration: 2000,
                          position: 'bottom'
                        });
                        toast1.present();
                        localStorage.removeItem("AlreadyClicked");
                      },
                        error => {
                          localStorage.removeItem("AlreadyClicked");
                          console.log(error);
                        });
                  } else {
                    if (that.checkedPass === 'PASSWORD_SET') {
                      this.askForPassword(d);
                      return;
                    }
                    that.serverLevelOnOff(d);
                  }
                }
              },
              {
                text: this.translate.instant('NO')
              }]
            });
            alert.present();
          },
            error => {
              this.apiCall.stopLoading();
              console.log("some error: ", error._body.message);
            });
      }
    }
  };

  checkImmobilizePassword() {
    // const rurl = this.apiCall.mainUrl + 'users/get_user_setting';
    // let Var = { uid: this.islogin._id };
    // this.apiCall.urlpasseswithdata(rurl, Var)
    //   .subscribe(data => {
    //     if (!data.engine_cut_psd) {
    //       this.checkedPass = 'PASSWORD_NOT_SET';
    //     } else {
    //       this.checkedPass = 'PASSWORD_SET';
    //     }
    //   })
    let that = this;
    if (!that.user_data.engine_cut_psd) {
      this.checkedPass = 'PASSWORD_NOT_SET';
    } else {
      this.checkedPass = 'PASSWORD_SET';
    }
  }

  askForPassword(d) {
    const prompt = this.alertCtrl.create({
      title: 'Enter Password',
      message: "Enter password for engine cut",
      inputs: [
        {
          name: 'password',
          placeholder: 'Password'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
          }
        },
        {
          text: 'Proceed',
          handler: data => {
            this.verifyPassword(data, d);
          }
        }
      ]
    });
    prompt.present();
  }
  toastmsg(msg) {
    this.toastCtrl.create({
      message: msg,
      duration: 1500,
      position: 'bottom'
    }).present();
  }
  verifyPassword(pass, d) {
    const ryurl = this.apiCall.mainUrl + "users/verify_EngineCut_Password";
    let payLd = {
      "uid": this.islogin._id,
      "psd": pass.password
    }
    this.apiCall.startLoading().present();
    this.apiCall.urlpasseswithdata(ryurl, payLd)
      .subscribe(resp => {
        this.apiCall.stopLoading();
        if (resp.message === 'password not matched') {
          this.toastmsg(resp.message)
          return;
        }
        this.serverLevelOnOff(d);
      },
        (err) => {
          this.apiCall.stopLoading();
        });
  }
  serverLevelOnOff(d) {
    localStorage.setItem("AlreadyClicked", "true");
    let data = {
      "imei": d.Device_ID,
      "_id": this.dataEngine._id,
      "engine_status": d.ignitionLock,
      "protocol_type": d.device_model.device_type
    }
    this.apiCall.serverLevelonoff(data)
      .subscribe(resp => {
        this.respMsg = resp;
        this.intervalID = setInterval(() => {
          this.apiCall.callResponse(this.respMsg._id)
            .subscribe(data => {
              this.commandStatus = data.status;
              if (this.commandStatus == 'SUCCESS') {
                clearTimeout(this.timeoutId);
                clearInterval(this.intervalID);
                localStorage.removeItem("AlreadyClicked");
                const toast1 = this.toastCtrl.create({
                  message: this.translate.instant('process has been completed successfully!'),
                  duration: 2000,
                  position: 'middle'
                });
                toast1.present();
              }
            })
        }, 5000);

        let that = this;
        that.timeoutId = setTimeout(() => {
          clearTimeout(that.timeoutId);
          clearInterval(that.intervalID);
          localStorage.removeItem("AlreadyClicked");
        }, that.timeoutSeconds);
      },
        err => {
          this.apiCall.stopLoading();
          localStorage.removeItem("AlreadyClicked");
        });
  }

  sharedevices(param) {
    let that = this;
    if (param == '15mins') {
      that.condition = 'gpsc';
      that.condition1 = 'light';
      that.condition2 = 'light';
      that.tttime = 15;
    } else {
      if (param == '1hour') {
        that.condition1 = 'gpsc';
        that.condition = 'light';
        that.condition2 = 'light';
        that.tttime = 60;
      } else {
        if (param == '8hours') {
          that.condition2 = 'gpsc';
          that.condition = 'light';
          that.condition1 = 'light';
          that.tttime = (8 * 60);
        }
      }
    }
  }

  shareLivetemp() {
    let that = this;
    if (that.tttime == undefined) {
      that.tttime = 15;
    }
    let data = {
      id: that.vehicleData._id,
      imei: that.vehicleData.Device_ID,
      sh: this.islogin._id,
      ttl: that.tttime   // set to 1 hour by default
    };
    this.apiCall.startLoading().present();
    this.apiCall.shareLivetrackCall(data)
      .subscribe(data => {
        this.apiCall.stopLoading();
        this.resToken = data.t;
        this.liveShare();
      },
        err => {
          this.apiCall.stopLoading();
          console.log(err);
        });
  }

  liveShare() {
    let that = this;
    let link = this.apiCall.mainUrl + "share/liveShare?t=" + that.resToken;
    TinyURL.shorten(link).then((res) => {
      that.socialSharing.share(that.islogin.fn + " " + that.islogin.ln + " has shared " + that.vehicleData.Device_Name + " live trip with you. Please follow below link to track", "OneQlik- Live Trip", "", res);

    })
    that.showFooter = false;
    that.tttime = undefined;
  }

  sharedVehicleDelete() {
    let that = this;
    let alert = that.alertCtrl.create({
      message: 'Do you want to delete this share vehicle ?',
      buttons: [{
        text: 'YES PROCEED',
        handler: () => {
          that.removeDevice(that.vehicleData._id);
        }
      },
      {
        text: 'NO'
      }]
    });
    alert.present();
  }

  removeDevice(did) {
    this.apiCall.startLoading().present();
    this.apiCall.dataRemoveFuncCall(this.islogin._id, did)
      .subscribe(() => {
        this.apiCall.stopLoading();
        let toast = this.toastCtrl.create({
          message: 'Shared Device was deleted successfully!',
          duration: 1500
        });
        toast.onDidDismiss(() => {
        });

        toast.present();
      },
        err => {
          this.apiCall.stopLoading();
          console.log(err)
        });
  }

  upload() {
    this.navCtrl.push('UploadDocPage', { vehData: this.vehicleData });
  }

  test(a, b) {
    if (b) {
      return !(b.split(",").indexOf(a) + 1);
    }
    else {
      return true;
    }
  }

  editItem() {
    let modal = this.modalCtrl.create('UpdateDevicePage', {
      vehData: this.vehicleData
    });
    modal.onDidDismiss(() => {
    })
    modal.present();
  }

  deleteItem() {
    let that = this;
    let alert = this.alertCtrl.create({
      message: 'Do you want to delete this vehicle ?',
      buttons: [{
        text: 'YES PROCEED',
        handler: () => {
          that.deleteDevice(that.vehicleData.Device_ID);
        }
      },
      {
        text: 'NO'
      }]
    });
    alert.present();
  }

  deleteDevice(d_id) {
    this.apiCall.startLoading().present();
    this.apiCall.deleteDeviceCall(d_id, this.islogin._id)
      .subscribe(data => {
        this.apiCall.stopLoading();
        let toast = this.toastCtrl.create({
          message: 'Vehicle deleted successfully!',
          position: 'bottom',
          duration: 2000
        });

        toast.onDidDismiss(() => {
        });

        toast.present();
      },
        err => {
          this.apiCall.stopLoading();
          let body = err._body;
          let msg = JSON.parse(body);
          let alert = this.alertCtrl.create({
            title: 'Oops!',
            message: msg.message,
            buttons: ['OK']
          });
          alert.present();
        });
  }

  shareItem() {
    let that = this;
    const prompt = this.alertCtrl.create({
      title: 'Share Vehicle',
      inputs: [
        {
          name: 'device_name',
          value: that.vehicleData.Device_Name
        },
        {
          name: 'shareId',
          placeholder: 'Enter Email Id/Mobile Number'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
          }
        },
        {
          text: 'Share',
          handler: data => {that.sharedevices(data)
          }
        }
      ]
    });
    prompt.present();
  }

  fonctionTest(d) {
    let theft;
    theft = !(d.theftAlert);
    if (theft) {
      let alert = this.alertCtrl.create({
        title: "Confirm",
        message: "Are you sure you want to activate anti theft alarm? On activating this alert you will get receive notification if vehicle moves.",
        buttons: [
          {
            text: 'YES PROCEED',
            handler: () => {
              let payload = {
                "_id": d._id,
                "theftAlert": theft
              }
              this.apiCall.startLoading();
              this.apiCall.deviceupdateCall(payload)
                .subscribe(data => {
                  this.apiCall.stopLoading();
                  let toast = this.toastCtrl.create({
                    message: "Anti theft alarm Activated!",
                    position: "bottom",
                    duration: 1000
                  });
                  toast.present();
                  this.callObjFunc(d);
                })
            }
          },
          {
            text: 'BACK',
            handler: () => {
            }
          }
        ]
      })
      alert.present();
    } else {
      let alert = this.alertCtrl.create({
        title: "Confirm",
        message: "Are you sure you want to deactivate anti theft alarm?",
        buttons: [
          {
            text: 'YES PROCEED',
            handler: () => {
              let payload = {
                "_id": d._id,
                "theftAlert": theft
              }
              this.apiCall.startLoading();
              this.apiCall.deviceupdateCall(payload)
                .subscribe(() => {
                  this.apiCall.stopLoading();
                  let toast = this.toastCtrl.create({
                    message: "Anti theft alarm Deactivated!",
                    position: "bottom",
                    duration: 1000
                  });
                  toast.present();
                  this.callObjFunc(d);
                })
            }
          },
          {
            text: 'BACK',
            handler: () => {}
          }
        ]
      })
      alert.present();
    }
  }

  showSharedBtn(a, b) {
    if (b) {
      return !(b.split(",").indexOf(a) + 1);
    }
    else {
      return true;
    }
  }

  showDeleteBtn(b) {
    let that = this;
    if (b) {
      let u = b.split(",");
      for (let p = 0; p < u.length; p++) {
        if (that.islogin._id == u[p]) {
          return true;
        }
      }
    }
    else {
      return false;
    }
  }

  shareLive() {
    let that = this;
    that.showFooter = true;
    that.drawerState1 = DrawerState.Docked;
  }

  getChart2() {
    function dynamicColors() {
      return "#f27c21";
    }

    function poolColors(a) {
      let pool = [];
      for (let i = 0; i < a; i++) {
        pool.push(dynamicColors());
      }
      return pool;
    }

    this.apiCall.get7daysData(this.vehicleData.Device_ID, new Date(this.curTime).toISOString())
      .subscribe(data => {
        let days = [];
        days = data.map(function (d) {
          let tempString = d._id['month'] + '/' + d._id['day'] + '/' + d._id['year'];
          return moment(new Date(tempString), 'DD/MM/YYYY').format('MMM-DD');
        })

        this.data7Days = days.map(function (d) {
          return d;
        })
        let total = 0;

        for (let i = 0; i < data.length; i++) {
          total += data[i].distance
        }
        let Data7 = [];
        Data7 = data.map(function (d) {
          return (d.distance / total) * 100;
        })

        this.data7Data = Data7.map(function (d) {
          return parseFloat(d).toFixed();
        });

        let that = this;

        if (that.barChart) {
          that.barChart.destroy();
        }
        that.barChart = new Chart(that.barCanvas.nativeElement, {
          type: 'bar',
          data: {
            labels: that.data7Days,
            datasets: [{
              data: that.data7Data,
              backgroundColor: poolColors(that.data7Data.length),
              borderColor: poolColors(that.data7Data.length),
              borderWidth: 1
            }]
          },
          options: {
            "hover": {
              "animationDuration": 0
            },

            "animation": {
              "duration": 1,
              "onComplete": function () {
                let chartInstance = this.chart,
                  ctx = chartInstance.ctx;

                ctx.font = Chart.helpers.fontString(10, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                ctx.textAlign = 'center';
                ctx.textBaseline = 'middle';

                this.data.datasets.forEach(function (dataset, i) {
                  let meta = chartInstance.controller.getDatasetMeta(i);
                  meta.data.forEach(function (bar, index) {
                    let data = dataset.data[index];
                    ctx.fillText(data, bar._model.x, bar._model.y - 5);
                  });
                });
              }
            },
            legend: {
              "display": false
            },
            tooltips: {
              // enabled: false
              position: 'nearest',
              mode: 'index',
              intersect: false,
              yPadding: 5,
              xPadding: 5,
              caretSize: 8,
              backgroundColor: 'rgba(72, 241, 12, 1)',
              titleFontColor: 'black',
              bodyFontColor: 'black',
              borderColor: 'rgba(0,0,0,1)',
              borderWidth: 2
            },
            plugins: {
              labels: {
                render: 'value',
                fontSize: 0,
                fontColor: '#fff',
              }
            },
            scales: {
              xAxes: [{
                stacked: true,
                // type: 'time',
                distribution: 'series',
                ticks: {
                  //this will fix your problem with NaN
                  // callback: function (label, index, labels) {
                  //     return label ? label : '';
                  // }
                  fontSize: 10,
                },
                gridLines: {
                  display: false
                },
                // font: '10px',
                barPercentage: 0.3,
                categoryPercentage: 1
              }],
              yAxes: [{
                display: false,
                ticks: {
                  beginAtZero: true,
                  // callback: function (label, index, labels) {
                  //     return label ? label : '';
                  // }
                },
                gridLines: {
                  display: true
                },
              }]
            }
          }
        });
      },
        err => {
          console.log("error found=> ", err);
        })
  }

  getChart() {
    let that = this;
    Chart.pluginService.register({
      beforeDraw: function (chart) {
        if (chart.config.options.elements.center) {
          //Get ctx from string
          let ctx = chart.chart.ctx;

          //Get options from the center object in options
          let centerConfig = chart.config.options.elements.center;
          let fontStyle = centerConfig.fontStyle || 'Arial';
          let txt = centerConfig.text;
          let color = centerConfig.color || '#000';
          let sidePadding = centerConfig.sidePadding || 20;
          let sidePaddingCalculated = (sidePadding / 100) * (chart.innerRadius * 2)
          //Start with a base font of 30px
          ctx.font = "30px " + fontStyle;

          //Get the width of the string and also the width of the element minus 10 to give it 5px side padding
          let stringWidth = ctx.measureText(txt).width;
          let elementWidth = (chart.innerRadius * 2) - sidePaddingCalculated;

          // Find out how much the font can grow in width.
          let widthRatio = elementWidth / stringWidth;
          let newFontSize = Math.floor(30 * widthRatio);
          let elementHeight = (chart.innerRadius * 2);

          // Pick a new font size so it will not be larger than the height of label.
          let fontSizeToUse = Math.min(newFontSize, elementHeight);

          //Set font settings to draw it correctly.
          ctx.textAlign = 'center';
          ctx.textBaseline = 'middle';
          let centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
          let centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);
          ctx.font = fontSizeToUse + "px " + fontStyle;
          ctx.fillStyle = color;

          //Draw text in center
          ctx.fillText(txt, centerX, centerY);
        }
      }
    });

    // if the chart is not undefined (e.g. it has been created)
    // then destory the old one so we can create a new one later
    if (that.doughnutChart) {
      that.doughnutChart.destroy();
    }
    let totalCount = 0;
    totalCount = that.vehicleData.speedChart["0-20"] + that.vehicleData.speedChart["20-40"] + that.vehicleData.speedChart["40-60"] + that.vehicleData.speedChart["60-80"] + that.vehicleData.speedChart["80-100"] + that.vehicleData.speedChart[">100"];
    let val1, val2, val3, val4, val5, val6;
    val1 = (that.vehicleData.speedChart["0-20"] / totalCount) * 100;
    val2 = (that.vehicleData.speedChart["20-40"] / totalCount) * 100;
    val3 = (that.vehicleData.speedChart["40-60"] / totalCount) * 100;
    val4 = (that.vehicleData.speedChart["60-80"] / totalCount) * 100;
    val5 = (that.vehicleData.speedChart["80-100"] / totalCount) * 100;
    val6 = (that.vehicleData.speedChart[">100"] / totalCount) * 100;

    that.doughnutChart = new Chart(that.doughnutCanvas.nativeElement, {

      type: 'doughnut',
      data: {
        labels: ["0 to 20", "20 to 40", "40 to 60", "60 to 80", "80 to 100", "100+"],
        datasets: [{
          label: 'My First dataset',
          data: [parseFloat(val1).toFixed(2), parseFloat(val2).toFixed(2), parseFloat(val3).toFixed(), parseFloat(val4).toFixed(), parseFloat(val5).toFixed(), parseFloat(val6).toFixed()],
          backgroundColor: [
            '#e0e63c', '#00a1e4', '#b9a44c', '#f5b700', '#f45b69', '#d600d6'
          ],
        }]
      },
      options: {
        plugins: {
          labels: {
            render: 'value',
            fontSize: 10,
            fontColor: '#fff',
          }
        },
        // pieceLabel: {
        //     render: 'label',
        //     fontColor: ['white', 'white', 'white', 'white', 'white'],
        //     position: 'outside',
        //     segment: true
        // },
        elements: {
          center: {
            text: "Speed Data (%)",
            color: '#d3d3d3', // Default is #000000
            fontStyle: 'Roboto', // Default is Arial
            fontSize: 0.1
            // sidePadding: 20 // Defualt is 20 (as a percentage)
          }
        },
        tooltips: {
          // enabled: false
          position: 'nearest',
          mode: 'index',
          intersect: false,
          yPadding: 10,
          xPadding: 10,
          caretSize: 8,
          backgroundColor: 'rgba(72, 241, 12, 1)',
          titleFontColor: 'black',
          bodyFontColor: 'black',
          borderColor: 'rgba(0,0,0,1)',
          borderWidth: 4
        },
        responsive: true,
        legend: {
          fontSize: 5,
          fontFamily: "tamoha",
          fontColor: "#fff",
          position: "bottom",

          labels: {
            fontColor: 'rgb(255, 99, 132)',
            fontSize: 7,
            // boxWidth: 5,
            usePointStyle: true
          }
        },
      }
    });
  }

  showDaily() {
    this.navCtrl.push('DailyReportPage', {
      param: this.vehicleData
    })
  }

  showStoppage() {
    this.navCtrl.push('StoppagesRepoPage', {
      param: this.vehicleData
    })
  }

  showTrip() {
    this.navCtrl.push('TripReportPage', {
      param: this.vehicleData
    })
  }

  showNotif() {
    this.navCtrl.push('AllNotificationsPage', {
      param: this.vehicleData
    })
  }

  showHistory() {
    this.navCtrl.push('HistoryDevicePage', {
      device: this.vehicleData
    })
  }

  showLive() {
    this.navCtrl.push('LiveSingleDevice', {
      device: this.vehicleData
    })
  }

  parseDuration(duration) {
    let remain = duration

    let days = Math.floor(remain / (1000 * 60 * 60 * 24))
    remain = remain % (1000 * 60 * 60 * 24)

    let hours = Math.floor(remain / (1000 * 60 * 60))
    remain = remain % (1000 * 60 * 60)

    let minutes = Math.floor(remain / (1000 * 60))
    remain = remain % (1000 * 60)

    let seconds = Math.floor(remain / (1000))
    remain = remain % (1000)

    let milliseconds = remain

    return {
      days,
      hours,
      minutes,
      seconds,
      milliseconds
    };
  }

  formatTime(o, useMilli = false) {
    let parts = []
    if (o.days) {
      let ret = o.days + ' day'
      if (o.days !== 1) {
        ret += 's'
      }
      parts.push(ret)
    }
    if (o.hours) {
      let ret = o.hours + ' hour'
      if (o.hours !== 1) {
        ret += 's'
      }
      parts.push(ret)
    }
    if (o.minutes) {
      let ret = o.minutes + ' minute'
      if (o.minutes !== 1) {
        ret += 's'
      }
      parts.push(ret)

    }
    if (o.seconds) {
      let ret = o.seconds + ' second'
      if (o.seconds !== 1) {
        ret += 's'
      }
      parts.push(ret)
    }
    if (useMilli && o.milliseconds) {
      let ret = o.milliseconds + ' millisecond'
      if (o.milliseconds !== 1) {
        ret += 's'
      }
      parts.push(ret)
    }
    if (parts.length === 0) {
      return 'instantly'
    } else {
      return parts.join(' ')
    }
  }

  formatTimeHMS(o) {
    let hours = o.hours.toString()
    if (hours.length === 1) hours = '0' + hours

    let minutes = o.minutes.toString()
    if (minutes.length === 1) minutes = '0' + minutes

    let seconds = o.seconds.toString()
    if (seconds.length === 1) seconds = '0' + seconds

    // return hours + ":" + minutes + ":" + seconds
    return hours + " hrs " + minutes
  }

  formatDurationHMS(duration) {
    let time = this.parseDuration(duration)
    return this.formatTimeHMS(time)
  }

  formatDuration(duration, useMilli = false) {
    let time = this.parseDuration(duration)
    return this.formatTime(time, useMilli)
  }

  towAlertCall(d) {
    let tow;
    tow = !(d.towAlert);
    if (tow) {
      let alert = this.alertCtrl.create({
        title: "Confirm",
        message: "Are you sure you want to activate Tow Alert alarm? On activating this alert you will get receive notification if vehicle has been towed.",
        buttons: [
          {
            text: 'YES PROCEED',
            handler: () => {
              let payload = {
                "_id": d._id,
                "towAlert": tow
              }
              this.apiCall.startLoading();
              this.apiCall.deviceupdateCall(payload)
                .subscribe(data => {
                  this.apiCall.stopLoading();
                  let toast = this.toastCtrl.create({
                    message: "Tow Alert alarm Activated!",
                    position: "bottom",
                    duration: 1000
                  });
                  toast.present();
                  this.callObjFunc(d);
                })
            }
          },
          {
            text: 'BACK'
          }
        ]
      })
      alert.present();
    } else {
      let alert = this.alertCtrl.create({
        title: "Confirm",
        message: "Are you sure you want to deactivate Tow Alert alarm?",
        buttons: [
          {
            text: 'YES PROCEED',
            handler: () => {
              let payload = {
                "_id": d._id,
                "towAlert": tow
              }
              this.apiCall.startLoading();
              this.apiCall.deviceupdateCall(payload)
                .subscribe(() => {
                  this.apiCall.stopLoading();
                  let toast = this.toastCtrl.create({
                    message: "Tow Alert alarm Deactivated!",
                    position: "bottom",
                    duration: 1000
                  });
                  toast.present();
                  this.callObjFunc(d);
                })
            }
          },
          {
            text: 'BACK',
            handler: () => {
              // d.theftAlert = !(d.theftAlert);
            }
          }
        ]
      })
      alert.present();
    }
  }

  callObjFunc(d) {
    let that = this;
    let _bUrl = this.apiCall.mainUrl + 'devices/getDevicebyId?deviceId=' + d.Device_ID;
    this.apiCall.startLoading().present();
    this.apiCall.getSOSReportAPI(_bUrl)
      .subscribe(resp => {
        this.apiCall.stopLoading();
        if (!resp) {
          return;
        } else {
          that.vehicleData = resp;
        }
      })
  }

}
