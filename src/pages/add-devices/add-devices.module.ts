import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddDevicesPage } from './add-devices';
import { SMS } from '@ionic-native/sms';
import { IonBottomDrawerModule } from '../../../node_modules/ion-bottom-drawer';
import { TranslateModule } from '@ngx-translate/core';
import { OnCreate } from './dummy-directive';
import { ProgressBarModule } from './progress-bar/progress-bar.module';
import { ModalModule } from 'ngx-bootstrap/modal';


@NgModule({
  declarations: [
    AddDevicesPage,
    OnCreate
  ],
  imports: [
    IonicPageModule.forChild(AddDevicesPage),
    TranslateModule.forChild(),
    IonBottomDrawerModule,
    ProgressBarModule,
    ModalModule.forRoot()
  ],
  exports: [
    OnCreate
  ],
  providers: [
    SMS,
  ]
})
export class AddDevicesPageModule {}
