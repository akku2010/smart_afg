import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DetailedPage } from './detailed/detailed';
import { ApiServiceProvider } from '../../providers/api-service/api-service';

@IonicPage()
@Component({
  selector: 'page-supported-devices',
  templateUrl: 'supported-devices.html',
})
export class SupportedDevicesPage {
  islogin: any;
  devices: any = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private apiCall: ApiServiceProvider) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
  }

  ionViewDidEnter() {
    console.log('ionViewDidEnter SupportedDevicesPage');
    this.getSupportedDevices();
  }

  showDetail(d) {
    console.log('check d here: ', d)
    this.navCtrl.push(DetailedPage, {
      param: d
    });
  }

  getSupportedDevices() {
    var url = this.apiCall.mainUrl + "product/getProduct?postedBy=59cbbdbe508f164aa2fef3d8";
    this.apiCall.startLoading().present();
    this.apiCall.getSOSReportAPI(url)
      .subscribe(data => {
        this.apiCall.stopLoading();
        console.log("response data: ", data);
        this.devices = data;
      },
        err => {
          this.apiCall.stopLoading();
          console.log("got error: ", err);
        });
  }

}
